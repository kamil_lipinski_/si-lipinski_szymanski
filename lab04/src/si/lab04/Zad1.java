package si.lab04;

public class Zad1 {

    public static void main(String[] args){
        int m = 0;
        String S = "¬(p → ¬q) ∨ r";
        //'∧' '∨' '→' '↔' '¬'
        System.out.println(S);
        System.out.println("Dla m = "+m+" wynik to: "+plTrue(S,m));
    }

    public static Integer plTrue(String S, Integer m){
        S = S.replace(" ","");
        if(S.length() != 1){
            int temp = 0;
            String S1 = "x";
            String S2 = "x";
            Character spojnik = 'x';
            int temp2 = 0;
            if(S.length() == 2 && S.charAt(0) == '¬'){
                if(m == 0){
                    return 1;
                }
                return 0;
            }
            if(S.indexOf("(") != -1){
                for (int i = 0; i < S.length(); i++){
                    if (S.charAt(i) == '('){
                        temp = i;
                    }
                    if (S.charAt(i) == ')'){
                        if (i == S.length()-1){
                            S1 = S.substring(0,temp-1);
                            S2 = S.substring(temp+1,i);
                            spojnik = S.charAt(temp-1);
                            if(S.charAt(temp-1) == '¬'){
                                S1 = S.substring(0,temp-2);
                                S2 = S.substring(temp+1,i);
                                spojnik = S.charAt(temp-2);
                                temp2 = 2;
                            }
                        }
                        else{
                            S1 = S.substring(temp+1,i);
                            S2 = S.substring(i+2);
                            spojnik = S.charAt(i+1);
                            if(temp == 1){
                                S1 = S.substring(temp+1,i);
                                S2 = S.substring(i+2);
                                spojnik = S.charAt(i+1);
                                temp2 = 1;
                            }
                        }
                    }
                }
            }
            else{
                for (int i = 0; i < S.length(); i++){
                    if(S.charAt(i) == '∧' || S.charAt(i) == '∨' || S.charAt(i) == '→' || S.charAt(i) == '↔'){
                        temp = i;
                    }
                }
                S1 = S.substring(0,temp);
                S2 = S.substring(temp+1);
                spojnik = S.charAt(temp);
            }
            if(spojnik == '∧' ){
                if(temp2 == 1){
                    if (plTrue(S1,m) == 0 && plTrue(S2,m) == 1){
                        return 1;
                    }
                    return 0;
                }
                else if(temp2 == 2){
                    if (plTrue(S1,m) == 1 && plTrue(S2,m) == 0){
                        return 1;
                    }
                    return 0;
                }
                if (plTrue(S1,m) == 1 && plTrue(S2,m) == 1){
                    return 1;
                }
                return 0;
            }
            if(spojnik == '∨' ){
                if(temp2 == 1){
                    if (plTrue(S1,m) == 0 || plTrue(S2,m) == 1){
                        return 1;
                    }
                    return 0;
                }
                else if(temp2 == 2){
                    if (plTrue(S1,m) == 1 || plTrue(S2,m) == 0){
                        return 1;
                    }
                    return 0;
                }
                if (plTrue(S1,m) == 1 || plTrue(S2,m) == 1){
                    return 1;
                }
                return 0;
            }
            if(spojnik == '→' ){
                if(temp2 == 1){
                    if (plTrue(S1,m) == 0 && plTrue(S2,m) == 0){
                        return 0;
                    }
                    return 1;
                }
                else if(temp2 == 2){
                    if (plTrue(S1,m) == 1 && plTrue(S2,m) == 1){
                        return 0;
                    }
                    return 1;
                }
                if (plTrue(S1,m) == 1 && plTrue(S2,m) == 0){
                    return 0;
                }
                return 1;
            }
            if(spojnik == '↔' ){
                if(temp2 == 1){
                    if ((plTrue(S1,m) == 1 || plTrue(S2,m) == 1) && (plTrue(S2,m) == 0 || plTrue(S1,m) == 0)){
                        return 1;
                    }
                    return 0;
                }
                else if(temp2 == 2){
                    if ((plTrue(S1,m) == 0 || plTrue(S2,m) == 0) && (plTrue(S2,m) == 1 || plTrue(S1,m) == 1)){
                        return 1;
                    }
                    return 0;
                }
                if ((plTrue(S1,m) == 0 || plTrue(S2,m) == 1) && (plTrue(S2,m) == 0 || plTrue(S1,m) == 1)){
                    return 1;
                }
                return 0;
            }
        }
        return m;
    }
}
