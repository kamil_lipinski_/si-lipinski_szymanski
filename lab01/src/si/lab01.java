package si;

import java.io.BufferedReader;
import java.io.FileReader;
import java.security.MessageDigest;
import java.util.*;

import static java.util.stream.Collectors.toCollection;

public class lab01 {
    public static void main(String[] args) throws Exception {
            System.out.println("Kamil Lipiński 155069 & Cezary Szymański 155966, grupa 3");

        //ZAD3
        String filePath = "C:\\Users\\Kamil\\Documents\\aaaaaaaaaastudia\\sztuczna_inteligencja\\lab01\\dane\\fertilityDiagnosis.txt";
        ArrayList<String> system = loadFile(filePath);

        //ZAD3 a)
        HashSet<String> decisionClasses = findDecisionClasses(system);
        System.out.println("\nZAD3 a)\nIstniejące w systemie symbole klas decyzyjnych: ");
        for (String i : decisionClasses) {
            System.out.println(i);
        }

        //ZAD3 b)
        HashMap<String, Integer> sizeOfDecisionClasses = findSizeOfDecisionClasses(system, decisionClasses);
        System.out.println("\nZAD3 b)\nWielkości klas decyzyjnych:");
        for (String i : sizeOfDecisionClasses.keySet()) {
            System.out.println(i + " - " + sizeOfDecisionClasses.get(i));
        }

        //ZAD3 c)
        String filePath2 = "C:\\Users\\Kamil\\Documents\\aaaaaaaaaastudia\\sztuczna_inteligencja\\lab01\\dane\\fertilityDiagnosis-type.txt";
        ArrayList<String> type = loadFile(filePath2);
        ArrayList<Integer> index = new ArrayList<>();
        for (int j = 0; j < type.size(); j++) {
            if (type.get(j).endsWith("n")) {
                index.add(j);
            }
        }
        HashMap<Integer, Double> min = findMinValue(system, index);
        HashMap<Integer, Double> max = findMaxValue(system, index);
        System.out.println("\nZAD3 c)\nMinimalne wartości poszczególnych atrybutów:");
        for (Integer i : min.keySet()) {
            System.out.println("a" + (i + 1) + " = " + min.get(i));
        }
        System.out.println("Maxymalne wartości poszczególnych atrybutów:");
        for (Integer i : max.keySet()) {
            System.out.println("a" + (i + 1) + " = " + max.get(i));
        }

        //ZAD3 d)
        HashMap<Integer, HashSet<String>> allAttributes = findAllAvailableValues(system, type);

        System.out.println("\nZAD3 d)\nLiczba różnych dostępnych wartości:");
        for (Integer i : allAttributes.keySet()) {
            System.out.println("a" + (i + 1) + " = " + allAttributes.get(i).size());
        }

        //ZAD3 e)
        System.out.println("\nZAD3 e)\nLista wszystkich różnych dostępnych wartości:");
        for (Integer i : allAttributes.keySet()) {
            System.out.println("a" + (i + 1) + " = " + allAttributes.get(i));
        }

        //ZAD3 f) w całym systemie
        HashMap<Integer, ArrayList<Double>> temp = SDprepWholeSystem(system, index);
        System.out.println("\nZAD3 f)\nOdchylenie standardowe dla poszczególnych atrybutów w całym systemie:");
        for (Integer i : temp.keySet()) {
            System.out.println("a" + (i + 1) + " = " + calculateSD(temp.get(i)));
        }

        //w klasach decyzyjnych
        HashMap<String, HashMap<Integer, ArrayList<Double>>> temp2 = SDprepForEachDecisionClass(system, index, decisionClasses);
        System.out.println("Odchylenie standardowe dla poszczególnych atrybutów w klasach decyzyjnych:");
        for (String i : temp2.keySet()) {
            System.out.println(i + ":");
            for (Integer j : temp2.get(i).keySet()) {
                System.out.println("a" + (j + 1) + " = " + calculateSD(temp2.get(i).get(j)));
            }
        }

        //ZAD4 a)
        System.out.println("\nZAD4 a) \nRandomowo wygenerowane 10 procent wartości nieznanych:");
        ArrayList<String> systemcp = loadFile(filePath);
        systemcp = generateRandom10Percent(systemcp, type);
        for (String i : systemcp){
            System.out.println(i);
        }

        //ZAD4 b)
        System.out.println("\nZAD4 b)");
        double a = -1;
        double b = 1;
        HashMap<Integer, ArrayList<Double>> normalized = normalized(a, b, system, index, min, max);
        System.out.println("Znormalizowane atrybuty numeryczne na przedziale od "+a+" do " +b);
        for (Integer i : normalized.keySet()) {
            System.out.println("a" + (i + 1) + " = " + normalized.get(i));
        }
        a = 0;b = 1;
        normalized = normalized(a, b, system, index, min, max);
        System.out.println("Znormalizowane atrybuty numeryczne na przedziale od "+a+" do " +b);
        for (Integer i : normalized.keySet()) {
            System.out.println("a" + (i + 1) + " = " + normalized.get(i));
        }
        a = -10;b = 10;
        normalized = normalized(a, b, system, index, min, max);
        System.out.println("Znormalizowane atrybuty numeryczne na przedziale od "+a+" do " +b);
        for (Integer i : normalized.keySet()) {
            System.out.println("a" + (i + 1) + " = " + normalized.get(i));
        }

        //ZAD4 c)
        HashMap<Integer, ArrayList<Double>> standardized = new HashMap<>();
        for (Integer j : index) {
            ArrayList<Double> essa = new ArrayList<>();
            double mean_a_i = mean(essa);
            double variance_a_i = variance(essa);
            for (String i : system) {
                double elo = (Double.valueOf(i.split(" ")[j]));
                elo = (elo-mean_a_i)/variance_a_i;
                essa.add(elo);
            }
            standardized.put(j, essa);
        }
        System.out.println("\nZAD4 c)\nUstandardyzowane atrybuty numeryczne:");
        for (Integer i : standardized.keySet()) {
            System.out.println(standardized.get(i));
        }
        //ZAD4 d)
        String filePath3 = "C:\\Users\\Kamil\\Documents\\aaaaaaaaaastudia\\sztuczna_inteligencja\\lab01\\dane\\Churn_Modelling.csv";
        ArrayList<String> data = loadFile(filePath3);
        String [][] data2 = reformatCSV(data);
        System.out.println("\nZadanie 4 d)");
        for(String [] i : data2) {
            for(String j : i)
                System.out.print(j+" ");
            System.out.println();
        }
    }
    public static ArrayList<String> loadFile (String filePath) throws Exception {
        BufferedReader bufReader = new BufferedReader(new FileReader(filePath));
        ArrayList<String> listOfLines = new ArrayList<>();
        String line = bufReader.readLine();
        while (line != null) {
            listOfLines.add(line);
            line = bufReader.readLine();
        }
        bufReader.close();
        return listOfLines;
    }

    public static HashSet <String> findDecisionClasses (ArrayList<String> system) {
        HashSet <String> decisionClasses = new HashSet <>();
        for (String i : system){
            decisionClasses.add(i.split(" ")[i.split(" ").length- 1]);
        }
        return decisionClasses;
    }

    public static HashMap <String, Integer> findSizeOfDecisionClasses (ArrayList<String> system, HashSet <String> decisionClasses) {
        HashMap <String, Integer> sizeOfDecisionClasses = new HashMap<>();
        for (String i : decisionClasses){
            int n = 0;
            for (String j : system){
                if (j.split(" ")[j.split(" ").length- 1].equals(i)){
                    n++;
                }
                sizeOfDecisionClasses.put(i, n);
            }
        }
        return sizeOfDecisionClasses;
    }

    public static HashMap <Integer,Double> findMinValue (ArrayList<String> system, ArrayList<Integer> index) {
        HashMap<Integer,Double> min = new HashMap<>();
        for (Integer j : index){
            min.put(j,Double.valueOf(system.get(0).split(" ")[j]));
        }
        for (String i : system){
            for (Integer j : index){
                double x = Double.valueOf(i.split(" ")[j]);
                if (x <= min.get(j)){
                    min.put(j,x);
                }
            }
        }
        return min;
    }

    public static HashMap <Integer,Double> findMaxValue (ArrayList<String> system, ArrayList<Integer> index) {
        HashMap<Integer,Double> max = new HashMap<>();
        for (Integer j : index){
            max.put(j,Double.valueOf(system.get(0).split(" ")[j]));
        }
        for (String i : system){
            for (Integer j : index){
                double x = Double.valueOf(i.split(" ")[j]);
                if (x >= max.get(j)){
                    max.put(j,x);
                }
            }
        }
        return max;
    }

    public static HashMap <Integer,HashSet<String>> findAllAvailableValues (ArrayList<String> system, ArrayList<String> type) {
        HashMap<Integer,HashSet<String>> allAttributes = new HashMap<>();
        for (int j = 0; j < type.size(); j++){
            HashSet <String> hs = new HashSet <>();
            for (String i : system){
                hs.add(i.split(" ")[j]);
            }
            allAttributes.put(j,hs);
        }
        return allAttributes;
    }

    public static double calculateSD (ArrayList<Double> values) {
        double sum = 0.0, standardDeviation = 0.0;
        int length = values.size();
        for(double num : values) {
            sum += num;
        }
        double mean = sum/length;
        for(double num: values) {
            standardDeviation += Math.pow(num - mean, 2);
        }
        return Math.sqrt(standardDeviation/length);
    }

    public static HashMap<Integer, ArrayList<Double>> SDprepWholeSystem (ArrayList<String> system ,ArrayList<Integer> index){
        HashMap<Integer, ArrayList<Double>> temp = new HashMap<>();
        for (Integer j : index){
            ArrayList<Double> values = new ArrayList<>();
            for (String i : system){
                values.add(Double.valueOf(i.split(" ")[j]));
            }
            temp.put(j, values);
        }
        return temp;
    }

    public static HashMap <String, HashMap<Integer, ArrayList<Double>>> SDprepForEachDecisionClass (ArrayList<String> system ,ArrayList<Integer> index, HashSet <String> decisionClasses){
        HashMap <String, HashMap<Integer, ArrayList<Double>>> temp2 = new HashMap<>();
        for (String i : decisionClasses){
            HashMap<Integer, ArrayList<Double>> tempx = new HashMap<>();
            for (Integer j : index){
                ArrayList<Double> values2 = new ArrayList<>();
                for (String k : system){
                    if (k.endsWith(i)){
                        values2.add(Double.valueOf(k.split(" ")[j]));
                    }
                }
                tempx.put(j, values2);
            }
            temp2.put(i,tempx);
        }
        return temp2;
    }

    public static ArrayList<String> generateRandom10Percent (ArrayList<String> system, ArrayList<String> type){
        int max1 = system.size()-1;
        int min1 = 0;
        int max2 = type.size()-1;
        int min2 = 0;
        int x = (system.size() * type.size())/10;
        for (int j = x; j > 0 ; j--){
            StringBuilder line = new StringBuilder();
            int rand1 = (int)(Math.random()*(max1-min1+1)+min1);
            int rand2 = (int)(Math.random()*(max2-min2+1)+min2);
            String temp3 = system.get(rand1).split(" ")[rand2];
            if (!temp3.equals("?")){
                String [] temp4 = system.get(rand1).split(" ");
                temp4[rand2] = "?";
                for (String k : temp4){
                    line.append(k+" ");
                }
                String line2 = line.substring(0,line.length()-1);
                system.set(rand1,line2);
            }
        }
        return system;
    }

    public static HashMap <Integer,ArrayList<String>> findMultipleValues (ArrayList<String> system, ArrayList<String> type) {
        HashMap <Integer,ArrayList<String>> allAttributes = new HashMap<>();
        for (int j = 0; j < type.size(); j++){
            ArrayList <String> hs = new ArrayList<>();
            for (String i : system){
                hs.add(i.split(" ")[j]);
            }
            allAttributes.put(j,hs);
        }
        return allAttributes;
    }

    public static HashMap<Integer, ArrayList<Double>> normalized(double a, double b, ArrayList<String> system, ArrayList<Integer> index, HashMap<Integer, Double> min, HashMap<Integer, Double> max) {
        HashMap<Integer, ArrayList<Double>> normalized = new HashMap<>();
        for (Integer j : index) {
            ArrayList<Double> essa = new ArrayList<>();
            for (String i : system) {
                double elo = (Double.valueOf(i.split(" ")[j]));
                elo = (((elo - min.get(j)) * (b - a)) / (max.get(j) - min.get(j))) + a;
                essa.add(elo);
            }
            normalized.put(j, essa);
        }

        return normalized;
    }

    public static double mean(ArrayList<Double> elo) {
        double sum = 0;
        for(double i : elo)
            sum += i;
        return sum / elo.size();
    }

    public static double variance (ArrayList<Double> elo) {
        double numerator = 0;
        double mean = mean(elo);
        for(double i : elo)
            numerator += ((i - mean) * (i - mean));
        return numerator / elo.size();
    }

    public static String [][] reformatCSV (ArrayList<String> data) {
        int index = 4;
        int rows = data.size();
        int objects = rows - 1;
        int columns = data.get(0).split(",").length;

        String [] array = new String[objects];
        for(int i = 1; i < rows; ++i) {
            array[i-1] = data.get(i).split(",")[index];
        }
        
        ArrayList<String> values = Arrays.stream(array).distinct().collect(toCollection(ArrayList::new));
        int columns2 = columns + values.size() - 2;
        String [][] newCSV = new String[rows][columns2];
        for(int i = 0; i < index; i++) {
            for(int j = 0; j < rows; ++j) {
                newCSV[j][i] = data.get(j).split(",")[i];
            }
        }
        for(int i = index+1; i < columns; i++) {
            for(int j = 0; j < rows; ++j) {
                newCSV[j][i+values.size()-2] = data.get(j).split(",")[i];
            }
        }
        for(int i = 1; i < values.size(); ++i) {
            newCSV[0][index-1+i] = "Geography."+values.get(i);
            for(int j = 1; j < rows; ++j) {
                if(data.get(j).split(",")[index].equals(values.get(i))) {
                    newCSV[j][index-1+i] = "1";
                } else
                    newCSV[j][index-1+i] = "0";
            }
        }
        return newCSV;
    }

}